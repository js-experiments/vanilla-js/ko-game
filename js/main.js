(function () {
  // Player factory
  function player({
    id,
    name
  }) {
    return {
      id,
      name,
    };
  }

  function generatePlayers(n) {
    return Array.from({
      length: n
    }, (empty, index) => player({
      id: index,
      name: `Player ${index + 1}`,
    }));
  }

  const app = document.querySelector('#app');
  const gameContainer = document.createElement('div');
  gameContainer.classList.add('game');
  let numRound = 1;
  let roundElements = {};

  function renderRound(round) {
    const {
      matches
    } = round;
    // Create a div for each round group
    const sides = {
      leftMatches: matches.slice(0, matches.length / 2),
      rightMatches: matches.slice(matches.length / 2),
    };

    // Create a div for each group match
    const elements = ['left', 'right']
      .map(side => createRoundColumn(sides[`${side}Matches`], round))
      .map((side, index) => {
        if (side) {
          const order = index === 0 ? numRound : game.rounds.length * 2 - numRound;
          side.style.order = order;
        }

        return side;
      })
      .forEach(side => side && gameContainer.appendChild(side));


    numRound++;
    return elements;
  }

  function createRoundColumn(matches, round) {
    if (!matches.length) return;

    const element = document.createElement('div');
    element.classList.add('game__round');
    element.style.transform = `perspective(100px) translateZ(-${(numRound - 1) * 40}px) scale(0.25)`;
    element.dataset.round = round.id;
    let title = `Round ${round.id}`;

    if (round.isFinal) {
      title = 'Finals';
      element.classList.add('--is-final');
    }

    element.innerHTML = `<h3 class="round__title">${title}</h3>`

    matches.forEach(match => renderMatch(match, element));

    if (!roundElements[round.id]) {
      roundElements[round.id] = [];
    }

    roundElements[round.id].push(element);

    return element;
  }

  function renderMatch(match, roundElement) {
    const matchElement = document.createElement('div');
    matchElement.classList.add('match');
    matchElement.dataset.key = match.id;

    const player1 = document.createElement('div');
    player1.classList.add('match__player');
    player1.textContent = match.players[0] ? match.players[0].name : '---';
    const player2 = document.createElement('div');
    player2.classList.add('match__player');
    player2.textContent = match.players[1] ? match.players[1].name : '---';

    matchElement.appendChild(player1);
    matchElement.appendChild(player2);

    roundElement.appendChild(matchElement);
  }

  const enableRound = (id) => {
    const elements = roundElements[id];
    elements.forEach(elm => elm.style.transform = 'perspective(100px) translateZ(0) scale(0.9)');
  }

  const disableRounds = (active) => {
    let count = 1;
    while (active >= 1) {
      const elements = roundElements[count++];
      elements.forEach(elm => elm.style.transform = `perspective(100px) translateZ(-${(active - 1) * 40}px) scale(0.25)`);
      active--;
    }
  }

  const handleRoundChange = (e, {
    previousRound,
    nextRound
  } = {}) => {
    console.log({
      previousRound,
      nextRound
    });
    if (!previousRound && !nextRound) {
      enableRound(1);
    }

    if (previousRound) {
      disableRounds(nextRound.id);
    }

    if (nextRound) {
      enableRound(nextRound.id);
    }
  };

  const game = kogame([...generatePlayers(16)])
    .create()
    .subscribe('game-started', handleRoundChange)
    .subscribe('match-over', (e, data) => console.log(e, data))
    .subscribe('update-match', (e, {
      match
    }) => {
      const matchElement = document.querySelector(`.match[data-key="${match.id}"]`);
      const playersElement = matchElement.querySelectorAll('.match__player');
      playersElement.forEach((player, index) => {
        player.textContent = match.players[index] ? match.players[index].name : '---';
      })
    })
    .subscribe('next-round', handleRoundChange)
    .subscribe('game-over', (e, data) => console.log(e, data));

  game.rounds.forEach((round) => {
    renderRound(round);
  });

  app.appendChild(gameContainer);

  game.start();
})(kogame);
