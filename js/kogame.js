let kogame;
// HELPER
// Checks if a number is a power of two
// 1000 = 2 ^ 3 = 8
// 0111 = 2 ^ 2 + 2 ^ 1 + 2 ^ 0 = 4 + 2 + 1 = 7
// 1000 & 0111 = 0000 = 0 == false
// 1010 & 1001 = 1000 = 8 == true (10 & 9)
function isPowerOfTwo(n) {
  return n > 0 && !(n & (n - 1));
}

(function () {
  let matchIds = 0;

  // Round factory
  function round(id, players, isFinal = false) {
    const matches = [];

    for (let i = 0; i < players.length / 2; i++) {
      matches.push(match(matchIds++, players.slice(i * 2, (i * 2) + 2)));
    }

    return {
      id,
      matches,
      isFinal,
      getWinners: () => matches.reduce((winners, match) => {
        winners.push(match.getWinner());
        return winners;
      }, []),
      isFinished: () => matches.every(match => match.getWinner()),
      updatePlayers: (matchIndex, player) => {
        const match = matches[parseInt(matchIndex / 2)];

        if (!match.players[0]) {
          match.players[0] = player;
        } else {
          match.players[1] = player;
        }

        return match;
      },
    }
  }

  // Match factory
  function match(id, players = []) {
    let winner;

    const setWinner = (playerIndex) => {
      winner = players.find((p, i) => i === playerIndex);
    }

    return {
      id,
      players,
      setWinner,
      getWinner: () => winner,
    }
  }

  kogame = function (players = []) {
    if (players.length < 2 && !isPowerOfTwo(players.length)) return;

    let intervalId;
    const subscribers = [];
    let currentRoundId = 0;
    const numRounds = Math.log2(players.length);
    const rounds = [];

    const createMatches = () => {
      let i = numRounds;
      while (i) {
        const roundPlayers = i === numRounds ? players.slice() : Array.from({
          length: 2 ** i
        });
        rounds.push(round((numRounds - i) + 1, roundPlayers, i === 1));
        i--;
      }
    };

    const currentRound = () => rounds.find(r => r.id === currentRoundId + 1);
    const nextRound = () => rounds.find(r => r.id === currentRoundId + 2);

    const startGame = () => {
      let matchIndex = 0;

      intervalId = setInterval(() => {
        const current = currentRound();
        const next = nextRound();
        const {
          matches
        } = current;
        const match = matches[matchIndex];
        const winnerIndex = randomWinner();
        match.setWinner(winnerIndex);

        if (next) {
          const toUpdate = next.updatePlayers(matchIndex, match.getWinner());
          notify('update-match', {
            match: toUpdate,
          });
        }

        matchIndex++;
        if (matchIndex === matches.length) {
          currentRoundId++;
          matchIndex = 0;
          const playing = currentRoundId < numRounds
          const event = playing ? 'next-round' : 'game-over';

          notify(event, {
            previousRound: current,
            nextRound: rounds[currentRoundId],
            winner: match.getWinner(),
          });

          return !playing && clearInterval(intervalId);
        }
      }, 1000);

      notify('game-started');
    };

    const randomWinner = () => Math.floor((Math.round(Math.random() * 12343431) / 11) % 2);

    const notify = (eventName, ...args) => subscribers
      .filter(e => e.eventName === eventName)
      .forEach(s => s.callback.apply(this, [eventName, ...args]));

    return {
      players,
      rounds,
      currentRound,
      create() {
        createMatches();
        return this;
      },
      start() {
        startGame();
        return this;
      },
      subscribe(eventName, callback) {
        subscribers.push({
          eventName,
          callback
        });

        return this;
      },
    }
  }
})();
